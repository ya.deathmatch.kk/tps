// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Types.h"
#include "Components/ArrowComponent.h"
#include "Projectiles.h"
#include "TPSInventoryComponent.h"

#include "WeaponDefault.generated.h"



DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, AnimReloadChar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, AnimFireChar);



UCLASS()
class TPS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponReloadStart OnWeaponReloadStart;


	FOnWeaponReloadEnd	OnWeaponReloadEnd;
	FOnWeaponFireStart OnWeaponFireStart;


	// ,         
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;    // 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;   // 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;  //  
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;   // 

	UPROPERTY(VisibleAnyWhere)
		FWeaponInfo WeaponSetting;  //  
	UPROPERTY(Replicated, EditAnyWhere, BlueprintReadWrite, Category = "Weapon Info")
		FAdditionalWeaponInfo AdditionalWeaponInfo;   //   


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void ShellDropTick(float DeltaTime);
	void ClipDropTick(float DeltaTime);
	void WeaponInit(); //








	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Firelogic")
		bool WeaponFiring = false;     //     
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Reloadlogic")
		bool WeaponReloading = false; //     
	bool WeaponAiming = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLofic")
		FName IdWeaponName;

	UFUNCTION(Server, Reliable, BlueprintCallable)
		void SetWeaponStateFire_OnServer(bool bIsFire);

	bool CheckWeaponCanFire();   //      
	FProjectileInfo GetProjectile();  //    
	void Fire();  //   


	UFUNCTION(Server, Reliable)
		void UpdateStateWeapon_OnServer(EMovementState NewMovementState);  //       
	void ChangeDispersionByShot();  //  - 
	float GetCurrentDispersion() const;
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;

	FVector GetFireEndLocation() const;
	int8 GetNumberProjectilesbyShot() const;









	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ReloadLogic")
		float FireTimer = 0.0f;  //  
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ReloadLogic")
		float ReloadTimer = 0.0f;//  

		// Remove Debug!!!!!
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ReloadLogic Debug")
		float ReloadTime = 0.0f;//  
	// flags
	bool BlockFire = false;
	//dispersion


	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentdispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	// Timer Drop magazine on reload
	bool DropClipFlag = false;
	float DropClipTimer = -1.0;

	// shell flag
	bool DropShellFlag = false;
	float DropShellTimer = -1.0f;


	UPROPERTY(Replicated)
		FVector ShootEndLocation = FVector(0);


	UFUNCTION(BlueprintCallable)
		int32 GetWeaponRound();
	UFUNCTION()
		void InitReload(); //   
	void FinishReload(); //  

	UFUNCTION(BlueprintCallable)
		void CancelReload();


	bool CheckCanWeaponReload();


	int8 GetAvailableAmmoForReload();


	UFUNCTION(Server, Reliable)
		void InitDropMesh_OnServer(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Debug")
		bool ShowDebug = false;
	//UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Debug")
		//bool byBarrel = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Debug")
		float SizeVectorToChangeShootDirectionLogic = 100.0f;


	UFUNCTION(Server, Unreliable)
		void UpdateWeaponByCharacterMovementState_OnServer(FVector NewShootEndLocation, bool NewShouldReduceDispersion);


	UFUNCTION(NetMulticast, Unreliable)
		void AnimWeaponStart_Multicast(UAnimMontage* Anim);

	UFUNCTION(NetMulticast, Unreliable)
		void ShellDropFire_Multicast(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDir);



	UFUNCTION(NetMulticast, Unreliable)
		void FXWeaponFire_Multicast(UParticleSystem* FxFire, USoundBase* SoundFire);



};
