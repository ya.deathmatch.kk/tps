	// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectiles.h"
#include "Types.h"
#include "Projectiles_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API AProjectiles_Grenade : public AProjectiles
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
	void TimerExplose(float Deltatime);
	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	
	virtual void ImpactProjectile() override;
	void Explose();



	bool TimerEnabled = false;
	float TimerToExplose = 0.0f;
	float TimeToExplose = 5.0f;

	
};
