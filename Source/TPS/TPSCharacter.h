// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Types.h"
#include "WeaponDefault.h"
#include "TPSInventoryComponent.h"
#include "TPSGameInstance.h"
#include "TPSCharacterHealthComponent.h"
#include "TPSPlayerController.h"
#include "TPS_IGameActor.h"
#include "TPSCharacterHealthComponent.h"
#include "TPSHealthComponent.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TPS_StateEffect.h"
#include "TPSCharacter.generated.h"


UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter, public ITPS_IGameActor //  public ITPS_IGameActor      Character
{
	GENERATED_BODY()

public:
	ATPSCharacter();


	FTimerHandle TimerHandle_RagdollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTPSInventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTPSHealthComponent* CharHealthComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTPSCharacterHealthComponent* CharShieldComponent;




private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UDecalComponent* CursorToWorld;




	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput(Id);
	}



public:

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.f, 40.f);

	UPROPERTY(Replicated)
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRunEnabled = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		bool  AimEnabled = false;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		bool bIsAlive = true;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		TArray<UAnimMontage*>DeadsAnim;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UTPS_StateEffect> AbilityEffect;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;
	UFUNCTION()   //   UFUNCTION   AXIS X
		void InputAxisX(float value); //       
	UFUNCTION()		//   UFUNCTION   AXIS Y
		void InputAxisY(float value);  //       
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();
	float AxisX = 0.f;
	float AxisY = 0.f;

	//Weapon
	UPROPERTY(Replicated)
		AWeaponDefault* CurrentWeapon = nullptr;



	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UDecalComponent* CurrentCursor = nullptr;
	// tick function

	//Effect
	TArray<UTPS_StateEffect*> Effects;




	UFUNCTION()
		void MovementTick(float DeltaTime); //         FPS

	UFUNCTION()
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable) // BlueprintCallable -     UE  Blueprint
		void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)//VisualOnly
		void RemoveCurrentWeapon();


	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();

	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);

	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake);
	UFUNCTION()
		bool TrySwitchWeaponToIndexByKeyInput(int32 ToIndex);
	UFUNCTION()
		void DropCurrentWeapon();

	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void DeadEvent_BP();



	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	UFUNCTION(BlueprintCallable)
		void TrySwitchNextWeapon();


	UFUNCTION(BlueprintCallable)
		void TrySwitchPreviousWeapon();

	// ability func
	void TryAbilityEnabled();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;


	//interface

	//bool AvailableForEffects_Implementation() override; //     ITPS_IGameActor   Implementation,
	//         


	EPhysicalSurface GetSurfaceType() override;

	TArray<UTPS_StateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTPS_StateEffect* RemoveEffect) override;
	void AddEffect(UTPS_StateEffect* newEffect) override;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool GetIsAlive();

	UFUNCTION()
		void CharDead();



	void EnableRagdoll();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;


	UFUNCTION(BlueprintNativeEvent)
		void CharDead_BP();

	UFUNCTION(Server, Unreliable)
		void SetActorRotationByYaw_OnServer(float Yaw);

	UFUNCTION(NetMulticast, Unreliable)
		void SetActorRotationByYaw_Multicast(float Yaw);



	UFUNCTION(Server, Reliable)
		void SetMovementState_OnServer(EMovementState NewState);

	UFUNCTION(NetMulticast, Reliable)
		void SetMovementState_Multicast(EMovementState NewState);


};

