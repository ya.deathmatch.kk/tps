// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSGameInstance.h"

bool UTPSGameInstance::GetWeaponInfoByName(FName NameWeapon,FWeaponInfo&OutInfo)
{

	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;

	WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
	if (WeaponInfoRow)
	{
		bIsFind = true;
		OutInfo = *WeaponInfoRow;


	}
	else
	{

	}

	return bIsFind;




}

bool UTPSGameInstance::GetDropIteminfoByWeaponName(FName NameItem, FDropItem& OutInfo)
{
	bool bIsFind = false;
	

	
	if (DropItemInfoTable)
	{


		FDropItem* DropItemInfoRow;
		TArray<FName>RowNames = DropItemInfoTable->GetRowNames();
		
		
		
		int8 i = 0;
		while (i<RowNames.Num()&&!bIsFind)
		{
			

			DropItemInfoRow= DropItemInfoTable->FindRow<FDropItem>(RowNames[i], "");
			if (DropItemInfoRow->WeaponInfo.NameItem == NameItem)
			{
				OutInfo = (*DropItemInfoRow);
				bIsFind = true;
			}
			i++;
		}

	}
	else
	{

	}

	return bIsFind;

}

bool UTPSGameInstance::GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo)
{

	bool bIsFind = false;
	FDropItem* DropItemInfoRow;
	if (DropItemInfoTable)
	{
		DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(NameItem, "", false);
		if (DropItemInfoRow)
		{
			bIsFind = true;
			OutInfo = *DropItemInfoRow;
		}
		else
		{

		}






	}





	return bIsFind;
}
	