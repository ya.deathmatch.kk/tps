// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"   // ��������� ��������� �������
#include "Chaos/ChaosEngineInterface.h"
#include "TPS_StateEffect.h"

#include "Types.generated.h"

// UPROPERTY - ������ ����������, ���� ��������, � ����� ������ � ����, � ���� � ����������
// UFUNCTION - �������, ������� ����� ������� ���� �� �������� � BP
// UCLASS - ����� (������ private, public ���������� ��������� )
// USTRUCT - ��������� (������ public, private ���������� ���������)

UENUM(BlueprintType)
enum class EMovementState :uint8
{
	Aim_State UMETA(DisplayName = "Aim State"),
	AimWalk_State UMETA(DisplayName = "AimWalk State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
	SprintRun_State UMETA(DisplayName = "SprintRun State"),


};


UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	RifleType UMETA(DisplayName = "Rifle"),
	ShotGunType UMETA(DisplayName = "ShotGun"),
	SniperRifle UMETA(DisplayName = "SniperRifle"),
	GrenadeLauncher UMETA(DisplayName = "GrenadeLauncher"),
	RocketLauncher UMETA(DisplayName = "RocketLauncher")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{

	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement") // ��� ������� ��������� ��� ������ ����������, �������
		float AimSpeedNormal = 200.f;												// �� ������� ��� ������ UE
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float WalkSpeedNormal = 300.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float RunSpeedNormal = 600.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float AimSpeedWalk = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SprintRunSpeedRun = 800.f;

};


USTRUCT(BlueprintType)
struct FProjectileInfo // Creating Projectile 
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		TSubclassOf< class AProjectiles>Projectile = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		UStaticMesh* ProjectileStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		FTransform ProjectileStaticMeshOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		UParticleSystem* ProjectileTrailFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		FTransform ProjectileTrailFxOffset = FTransform();



	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		float ProjectileLifeTime = 20.f;	// ����� ����� ����
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		float ProjectileDamage = 20.f;  // ����
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		float ProjectileInitSpeed = 2000.f;  // �������� ����


	 //material to decal on hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX") 
		TMap <TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	//Sound when hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		USoundBase* HitSound = nullptr; //�������� � ���� ��������� ����������� ������� ������������� nullptr ��������, ��� ���� �� ��������� � ��� ���� ��������� �� ��������� ������ � ����
	//fx when hit check by surface
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		TMap <TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;

		
	// Hit fx Actor?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
		TSubclassOf<UTPS_StateEffect> Effect = nullptr;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode") 
		UParticleSystem* ExplodeFX = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		USoundBase* ExplodeSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ProjectileMinRadiusDamage = 200.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ProjectileMaxRadiusDamage = 200.f;  // ��������� ������ ���� ������� � �������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ExplodeMaxDamge = 40.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ExplodeFalloffCoef = 1.0f;
	
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
	//	bool bIsLikeBomp = false;	// ��������� ������� ����������, ������� ������ Bomp � false


};



USTRUCT(BlueprintType)  // ��������� ������
struct FWeaponDispersion
{

	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")  
		float DispersionAimStart = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float DispersionAimMax = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float DispersionAimMin = 0.1f;


		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float DispersionAimShoot = 1.0f;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float Aim_StateDispersionAimMax = 2.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float  Aim_StateDispersionAimMin = 0.3f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float  Aim_StateDispersionAimRecoil = 1.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float  Aim_StateDispersionReduction = 0.3f;


		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float  AimWalk_StateDispersionAimMax = 1.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float  AimWalk_StateDispersionAimMin = 0.1f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float  AimWalk_StateDispersionAimRecoil = 1.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float  AimWalk_StateDispersionReduction = 0.4f;



		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float  Walk_StateDispersionAimMax = 5.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float  Walk_StateDispersionAimMin = 1.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float  Walk_StateDispersionAimRecoil = 1.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float  Walk_StateDispersionReduction = 0.2f;


		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float  Run_StateDispersionAimMax = 10.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float  Run_StateDispersionAimMin = 4.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float  Run_StateDispersionAimRecoil = 1.0f;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
			float  Run_StateDispersionReduction = 0.1f;


};




USTRUCT(BlueprintType)
struct FAnimationWeaponInfo  // Animation Structure
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
		UAnimMontage* AnimCharFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
		UAnimMontage* AnimCharFireAnim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
		UAnimMontage* AnimCharReload = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
		UAnimMontage* AnimCharReloadAim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Weapon")
		UAnimMontage* AnimWeaponReload = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Weapon")
		UAnimMontage* AnimWeaponReloadAim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Weapon")
		UAnimMontage* AnimWeaponFire = nullptr;


};






USTRUCT(BlueprintType)
struct FDropMeshInfo    //creating ctructure with meshes, which we shall drop when reload weapon

{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		UStaticMesh* DropMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		float DropMeshTime = -1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		float DropMeshLifeTime  =  5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		FTransform DropMeshOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		FVector DropMeshImpulseDir = FVector(0.0f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		float PowerImpulse = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		float ImpulseRandomDispersion = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drop Mesh")
		float CustomMass = 0.0f;




};

USTRUCT(BlueprintType)
struct FWeaponInfo:public FTableRowBase // ������� ������� � ��������� �� ���, ����� ��� ������ ����� ���
{
	GENERATED_BODY()


		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
		TSubclassOf< class AWeaponDefault>WeaponClass = nullptr;

	

		
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float RateofFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float ReloadTime = 2.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "State")
		int32 MaxRound = 10;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "State")
		int32 NumberProjectilesByShot = 1;



	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
		FWeaponDispersion DispersionWeapon;  // ������ � ������� ������ ��������� FWeaponDispersion � �������� �� DispersionWeapon

	
		

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Sound")  //����� ��������� � �����������
		USoundBase* SoundFireWeapon = nullptr;	
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundReloadWeapon = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "FX")	// ������� ������ ������� ������
		UParticleSystem* EffectFireweapon = nullptr;  
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Projectile")
		FProjectileInfo ProjectileSetting; // ������ � ������� ������ ��������� FProjectileInfo � �������� ProjectileSetting



	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
	//	float WeaponDamage = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float DistanceTrace = 2000.f;

	// hit effect one decal for all?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitEffect")
		UDecalComponent* DecalOnHit = nullptr;


	// ��������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
		FAnimationWeaponInfo AnimWeaponInfo;
	//	UAnimMontage* AnimCharFire = nullptr;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	//	UAnimMontage* AnimCharReload = nullptr;

	// meshes for bulletes and magazines
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		FDropMeshInfo ClipDropMesh;
	//	UStaticMesh* MagazineDrop = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		FDropMeshInfo ShellBullets;  //�� ����� ������� ��� � ���� �������� � �������, ��� ����
	// ��������� �� ���� � �� �� ��������� FDropMeshInfo
	//	UStaticMesh* SleeveBullets = nullptr;
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		float SwitchTimeToWeapon = 1.0f;		// ����� ������������ ������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		UTexture2D* WeaponIcon = nullptr;  // ������� ���� ��� ������ ������

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		EWeaponType WeaponType = EWeaponType::RifleType;








};




USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()

		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Weapon Stats")
		int32 Round = 10; // ���������� � �������� ��������

};


USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
		FName NameItem;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
		FAdditionalWeaponInfo AdditionalInfo;
};




USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

		///Index Slot by Index Array
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		EWeaponType WeaponType = EWeaponType::RifleType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		int32 Cout = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		int32 MaxCout = 100;
};



//USTRUCT(BlueprintType)
//struct FDropItem:public FTableRowBase
//{
//	GENERATED_BODY()
//
//		Index Slot by Index Array
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
//		UStaticMesh*WeaponStaticMesh = nullptr;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
//		USkeletalMesh* WeaponSkeletalMesh = nullptr;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
//		FWeaponInfo WeaponInfo;
//};

USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase
{
	GENERATED_BODY()

		///Index Slot by Index Array
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
		UStaticMesh* WeaponStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
		USkeletalMesh* WeaponSkeletMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
		UParticleSystem* ParticleItem = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
		FTransform Offset;





	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
		FWeaponSlot WeaponInfo;






};






UCLASS()
class TPS_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()



public:


	UFUNCTION(BlueprintCallable)
	static void AddEffectBySurfaceType(AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<UTPS_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType);



};

